// Include the ThinkBox library.
#include "ThinkBox.h"

// CONFIGURATION
// Pin connections:
#define BUTTON_A_PIN    D6  // Pin number for capsense button which sends to channel A. INPUT.
#define BUTTON_B_PIN    D7  // Pin number for capsense button which sends to channel B. INPUT.
#define BUTTON_C_PIN    D8  // Pin number for capsense button which sends to channel C. INPUT.
#define BUTTON_OUT_PIN  D5  // Pin number for output signal to capacitative sensors. OUTPUT

#define POT_PIN         A0  // Pin number for the 10K potentiometer which controls LED brightness. INPUT.

#define PIXEL_PIN       3   // NeoPixelBus hardcodes this to GPIO3/RX for ESP8266.
#define PIXEL_COUNT     1   // Number of neopixels in total.

#define STATUS_R_PIN    D3  // Pin number for red status LED. OUTPUT.
#define STATUS_G_PIN    D4  // Pin number for green status LED. OUTPUT.

#define POWER_PIN       D8  // Pin number for power control. INPUT.

// Other settings:
#define DEBUG           1   // Set to 1 to enable debug in serial monitor.

/*
const char channel_owner[] = "feeds/thinkbox-luca";
const int channel_owner_leds[] = {3, 4};
const char channel_a[] = "feeds/thinkbox-theodor";
const int channel_a_led = 1;
const char channel_b[] = "feeds/thinkbox-kaya";
const int channel_b_led = 2;
const char channel_c[] = "feeds/thinkbox-lilje";
const int channel_c_led = 5;

const char *_aio_server = "io.adafruit.com";
uint16_t _aio_serverport = 1883;
const char *_aio_username = "achton";
const char *_aio_key = "e4e8cb1477894f64bd5ff450da8f842a";
*/
// END CONFIGURATION


// Instantiate ThinkBox object.
ThinkBox thinkbox (
  PIXEL_COUNT, PIXEL_PIN,
  DEBUG
);

// Core setup method.
void setup() {
  if (DEBUG == 1) {
    Serial.begin(115200);
    Serial.println("Debug enabled.");
  }
  thinkbox.initialize();
}

// Core loop method.
void loop() {
  thinkbox.run();
}

