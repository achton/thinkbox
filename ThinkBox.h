// WiFi and web prerequisites - https://github.com/esp8266/Arduino/tree/master/libraries
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
// EEPROM interface library
//#include <EEPROM.h>
// WiFiManager (https://github.com/tzapu/WiFiManager)
#include <WiFiManager.h>
// MQTT connector for io.adafruit.com
#include <Adafruit_MQTT_Client.h>
#include <Adafruit_MQTT.h>
// RBD libraries for capacitative sensors - http://robotsbigdata.com/docs-arduino-human-sensor.html
#include <RBD_Capacitance.h>
#include <RBD_Threshold.h>
#include <RBD_HumanSensor.h>
// NeoPixel LED library - https://github.com/Makuna/NeoPixelBus
#include <NeoPixelBus.h>
#include <NeoPixelAnimator.h>
// For LED status (Ticker is a esp8266 library that calls functions periodically) - https://github.com/esp8266/Arduino/tree/master/libraries/Ticker
#include <Ticker.h>


class ThinkBox {
  public:
    ThinkBox(const int pixel_count, const int pixel_pin, 
      const int debug);
    void initialize();
    void run();
    void setDebug(int debug);  
    bool isDebug();  

    //Adafruit_MQTT_Publish ownerFeed;

  private:
    void setLedPin(int pin);
    void setPotPin(int pin);
    bool publishTriggered(uint8_t feed);
    void MQTT_connect();
    void tick();
    bool MQTTConnected();
    bool wifiConnected();
    void FadeOutAnimUpdate(const AnimationParam& param);
    void LoopAnimUpdate(const AnimationParam& param);
        
    bool _is_debug;
    int _led_r_pin;
    int _led_g_pin;
    int _pixels_pin;
    int _pot_pin;

    char _channel_owner[];

    const char *_aio_server = "io.adafruit.com";
    uint16_t _aio_serverport = 1883;
    const char *_aio_username = "achton";
    const char *_aio_key = "e4e8cb1477894f64bd5ff450da8f842a";

    uint16_t AnimCount; // we only need enough animations for the tail and one extra
    const uint16_t PixelFadeDuration = 300; // third of a second
    // one second divide by the number of pixels = loop once a second
    uint16_t NextPixelMoveDuration; // how fast we move through the pixels
    NeoGamma<NeoGammaTableMethod> colorGamma; // for any fade animations, best to correct gamma
    NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> pixels;
    NeoPixelAnimator animations; // NeoPixel animation management object

    struct animationState {
      RgbColor StartingColor;
      RgbColor EndingColor;
      uint16_t IndexPixel; // which pixel this animation is effecting
    };
   


    Ticker ticker;
    
    WiFiClient wifiClient;
    
    Adafruit_MQTT_Client mqtt;
    Adafruit_MQTT_Subscribe ownerFeedSub;
    Adafruit_MQTT_Publish FeedAPublisher;
    Adafruit_MQTT_Publish FeedBPublisher;
    Adafruit_MQTT_Publish FeedCPublisher;
    
};

