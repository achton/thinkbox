#include "ThinkBox.h"

const char channel_owner[] = "feeds/thinkbox-luca";
const int channel_owner_leds[] = {3, 4};
const char channel_a[] = "feeds/thinkbox-theodor";
const int channel_a_led = 1;
const char channel_b[] = "feeds/thinkbox-kaya";
const int channel_b_led = 2;
const char channel_c[] = "feeds/thinkbox-lilje";
const int channel_c_led = 5;

void wifiAPCallback(WiFiManager* wifi) {
  Serial.println("wifiAPCallback called.");
  // Todo: blink an LED here during AP config portal setup.
}

/*
 * Class constructor.
 */
ThinkBox::ThinkBox(const int pixel_count, const int pixel_pin, const int debug)
  : mqtt(&wifiClient, _aio_server, _aio_serverport, _aio_username, _aio_key),
    ownerFeedSub(&mqtt, channel_owner),
    FeedAPublisher(&mqtt, channel_a),
    FeedBPublisher(&mqtt, channel_b, 1),
    FeedCPublisher(&mqtt, channel_c, 1),
    pixels(pixel_count, pixel_pin),
    AnimCount(pixel_count / 5 * 2 + 1),
    animations(AnimCount),
    NextPixelMoveDuration(1000 / pixel_count)
{
  setDebug(debug);
//  _channel_owner = channel_owner;
}

void ThinkBox::initialize() {

  if (isDebug()) {
    Serial.println();
    Serial.println("Initializing ThinkBox...");
  }

  randomSeed(analogRead(0));

  // Turn off all pixels.
  pixels.Begin();
  pixels.Show();

  // Initialize WiFiManager
  //reset settings - for testing
  //wifiManager.resetSettings();
  WiFiManager wifiManager;

  wifiManager.setDebugOutput(isDebug());
  //wifiManager.setAPStaticIPConfig(IPAddress(192,168,1,1), IPAddress(10,0,1,1), IPAddress(255,255,255,0));
  //wifiManager.setMinimumSignalQuality(10);

  // This is called when AP config portal successfully connects.
  //wifiManager.setAPCallback(wifiAPCallback);

  // Configure extra parameter to make editable during AP setup.
  //WiFiManagerParameter custom_channel_name("channel", "channel name", channel_name, 40);
  //wifiManager.setAPCallback(configModeCallback);
  
  //wifiManager.addParameter(&custom_channel_name);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect("_TankeBox")) {
    Serial.println("failed to connect and hit timeout");
    delay(10000);
    //reset and try again, or maybe put it to deep sleep
    //ESP.reset();
    //delay(5000);
  }

  // if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");

  // TODO: save the custom parameters to EEPROM
  //if (saveWiFiCustomConfig) {
    //Serial.println("saving config");
  //}

  if (wifiConnected()) {
    if (isDebug()) {
      Serial.println("Box is connected to wifi.");
    }

    // Set up the MQTT client.
    Adafruit_MQTT_Client mqtt(&wifiClient, _aio_server, _aio_serverport, _aio_username, _aio_key);
    
    // Subscribe to the owner feed.
    Adafruit_MQTT_Subscribe ownerFeedSub = Adafruit_MQTT_Subscribe(&mqtt, channel_owner);
    mqtt.subscribe(&ownerFeedSub);
    
    // Set up publishers to multiple feeds using QOS 1.
    //const char PHOTOCELL_FEED[] PROGMEM = AIO_USERNAME "/feeds/thinkbox-theodor";
    Adafruit_MQTT_Publish FeedAPublisher = Adafruit_MQTT_Publish(&mqtt, channel_a, 1);
    //const char ONOFF_FEED[] PROGMEM = AIO_USERNAME "/feeds/thinkbox-kaya";
    Adafruit_MQTT_Publish FeedBPublisher = Adafruit_MQTT_Publish(&mqtt, channel_b, 1);
    Adafruit_MQTT_Publish FeedCPublisher = Adafruit_MQTT_Publish(&mqtt, channel_c, 1);

  }

  if (isDebug()) {
    Serial.println("Done initializing ThinkBox.");
  }

}


void ThinkBox::run() {

  if (isDebug()) {
    Serial.print(".");
  }
  
  MQTT_connect();

  delay(2000);

  if (isDebug()) {
    Serial.print("+");
    Serial.println("Reading MQTT subscription...");
  }

  // this is our 'wait for incoming subscription packets' busy subloop
  // try to spend your time here
  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(5000))) {
    // Check if its the owner feed
    if (subscription == &ownerFeedSub) {
      Serial.print(F("Owner feed: "));
      Serial.println((char *)ownerFeedSub.lastread);

      // TODO: light the pixels!
      //pixels.
      if (strcmp((char *)ownerFeedSub.lastread, "ON") == 0) {
        digitalWrite(BUILTIN_LED, LOW); 
      }
      if (strcmp((char *)ownerFeedSub.lastread, "OFF") == 0) {
        digitalWrite(BUILTIN_LED, HIGH); 
      }
    }
  }
  if (isDebug()) {
    Serial.println("Subscription checking done.");
  }

  // Now we can check if we need to publish stuff!
  if (publishTriggered(1)) {
    Serial.print(F("\nSending FeedAPublisher val to "));
    Serial.println(channel_a);
  
    if (! FeedAPublisher.publish("nanan")) {
      Serial.println(F("Failed"));
    } else {
      Serial.println(F("OK!"));
    }
  }


  // ping the server to keep the mqtt connection alive
  if(! mqtt.ping()) {
    if (isDebug()) {
      Serial.println("Pinging MQTT.");
    }  
    mqtt.disconnect();
  }

  delay(500);
  
}

bool ThinkBox::wifiConnected() {

  Serial.println("\nStarting connection to server...");
  Serial.println("");
  // if you get a connection, report back via serial:
  char server[] = "www.amazon.com";
  if (wifiClient.connect(server, 80)) {
    Serial.println("connected to server");
    // Make a HTTP request:
    wifiClient.println("GET / HTTP/1.1");
    wifiClient.println("Host: www.amazon.com");
    wifiClient.println("Connection: close");
    wifiClient.println();
    while (wifiClient.connected()) {
      String line = wifiClient.readStringUntil('\n');
      Serial.println(line);
      if (line == "\r") {
        Serial.println("headers received");
        break;
      }
    }
  } else {
    Serial.println("ERROR: Could not connect to server on port 80.");
    return false;
  }
  WiFi.printDiag(Serial);
  
  return (WiFi.status() == WL_CONNECTED);
}

// Private

bool ThinkBox::publishTriggered(uint8_t feed_id) {
  // TODO: check here for whether the capsense button was triggered.
  Serial.print("Checking if publish was triggered for feed ");
  Serial.println(feed_id);
  
  int rnd = random(300);
  Serial.println(rnd);
  return (rnd > 250);
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void ThinkBox::MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  if (isDebug()) {
    Serial.println("Connecting to MQTT... ");
  }

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
    if (isDebug()) {
      Serial.println(mqtt.connectErrorString(ret));
      Serial.println("Retrying MQTT connection in 5 seconds...");
    }
    mqtt.disconnect();
    delay(5000);  // wait 5 seconds
    retries--;
    if (retries == 0) {
      if (isDebug()) {
        Serial.println("ERROR: Could not reconnect to MQTT after X retries.");
      }  
    }
  }

  if (isDebug() && mqtt.connected()) {
    Serial.println("MQTT connected.");
  }
}

void ThinkBox::setDebug(int debug) {
  if (debug == 0) {
    _is_debug = false;
  } else {
    _is_debug = true;
  }
}

bool ThinkBox::isDebug() {
  return _is_debug;
}

void ThinkBox::tick() {
  int pin = _led_g_pin;
  int state = digitalRead(pin);
  //toggle state
  digitalWrite(pin, !state);
}

void ThinkBox::FadeOutAnimUpdate(const AnimationParam& param)
{
    // this gets called for each animation on every time step
    // progress will start at 0.0 and end at 1.0
    // we use the blend function on the RgbColor to mix
    // color based on the progress given to us in the animation
    RgbColor updatedColor = RgbColor::LinearBlend(
        ThinkBox::animationState[param.index].StartingColor,
        ThinkBox::animationState[param.index].EndingColor,
        param.progress);

    // apply the color to the strip
    pixels.SetPixelColor(ThinkBox::animationState[param.index].IndexPixel, 
        colorGamma.Correct(updatedColor));
}

void ThinkBox::LoopAnimUpdate(const AnimationParam& param)
{
    // wait for this animation to complete,
    // we are using it as a timer of sorts
    if (param.state == AnimationState_Completed)
    {
        // done, time to restart this position tracking animation/timer
        ThinkBox::animations.RestartAnimation(param.index);

        // pick the next pixel inline to start animating
        // 
        frontPixel = (frontPixel + 1) % PixelCount; // increment and wrap
        if (frontPixel == 0)
        {
            // we looped, lets pick a new front color
            frontColor = HslColor(random(360) / 360.0f, 1.0f, 0.25f);
        }

        uint16_t indexAnim;
        // do we have an animation available to use to animate the next front pixel?
        // if you see skipping, then either you are going to fast or need to increase
        // the number of animation channels
        if (ThinkBox::animations.NextAvailableAnimation(&indexAnim, 1))
        {
            ThinkBox::animationState[indexAnim].StartingColor = frontColor;
            ThinkBox::animationState[indexAnim].EndingColor = RgbColor(0, 0, 0);
            ThinkBox::animationState[indexAnim].IndexPixel = frontPixel;

            ThinkBox::animations.StartAnimation(indexAnim, PixelFadeDuration, FadeOutAnimUpdate);
        }
    }
}

